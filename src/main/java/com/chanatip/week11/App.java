package com.chanatip.week11;


public class App 
{
    public static void main( String[] args )
    {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        bird1.run();

        System.out.println("------------------------------");

        Plane boeing = new Plane("boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();

        System.out.println("------------------------------");

        Superman clark = new Superman("Clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        clark.swim();

        System.out.println("------------------------------");

        Bat bat1 = new Bat("Bruce Wayne");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        System.out.println("------------------------------");

        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();

        System.out.println("------------------------------");

        Rat rat1 = new Rat("Remy");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();

        System.out.println("------------------------------");

        Dog dog1 = new Dog("Elon");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        dog1.swim();

        System.out.println("------------------------------");

        Cat cat1 = new Cat("Garfield");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();

        System.out.println("------------------------------");

        Fish fish1 = new Fish("Salmon");
        fish1.eat();
        fish1.sleep();
        fish1.swim();

        System.out.println("------------------------------");

        Crocodile crocodile1 = new Crocodile("Bob");
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();
        crocodile1.crawl();

        System.out.println("------------------------------");

        Snake snake1 = new Snake("BigBoss");
        snake1.crawl();
        snake1.eat();
        snake1.sleep();

        System.out.println("------------------------------");
        System.out.println("-----------flyables-----------");

        Flyable[] flyables = {bird1, boeing, clark, bat1};
        for(int i=0; i<flyables.length;i++){
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        System.out.println("------------------------------");
        System.out.println("----------Walkables-----------");

        Walkable[] walkables = {bird1, clark, man1, cat1, dog1, rat1,};
        for(int i=0; i<walkables.length;i++){
            walkables[i].walk();
            walkables[i].run();
        }

        System.out.println("------------------------------");
        System.out.println("----------Swimables-----------");

        Swimable[] swimables = {fish1, clark, man1, dog1, crocodile1};
        for(int i=0; i<swimables.length;i++){
            swimables[i].swim();
        }

        System.out.println("------------------------------");
        System.out.println("----------Crawlables----------");

        Crawlable[] crawlables = {snake1, crocodile1};
        for(int i=0; i<crawlables.length;i++){
            crawlables[i].crawl();
        }
    }
}
