package com.chanatip.week11;

public abstract class Vehical {
    private String name;
    private String engine;
    public Vehical(String name, String engine){
        this.name = name;
        this.engine = engine;
    }
    public String getName(){
        return this.name;
    }
    public String getEngine(){
        return this.engine;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setNumberOfLeg(String engine){
        this.engine = engine;
    }
}
