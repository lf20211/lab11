package com.chanatip.week11;

public class Plane extends Vehical implements Flyable{

    public Plane(String name, String engine) {
        super(name, engine);
    }

    @Override
    public void takeoff() {
        System.out.println(this + "takeoff.");
    }

    @Override
    public void fly() {
        System.out.println(this + "fly.");
    }

    @Override
    public void landing() {
        System.out.println(this + "landing.");
    }

    @Override
    public String toString() {
        return "Plane ("+getName()+") ";
    }
}
